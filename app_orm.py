from models.books_orm import Database
from flask import Flask, request
import json
from flask.json import jsonify

app = Flask(__name__)
app.config["JSON_SORT_KEYS"] = False

@app.route("/")
def main():
    try:
        message = {
            "msg": "aplikasi sukses"
        }
        return jsonify(message)
    except Exception as e:
        print(f"kesalahan main: {e}")

@app.route("/books", methods=["GET", "POST"])
def showBook():
    try:
        if request.method == "POST":
            try:
                params = request.json
                return db.showBooksByParams(**params)
            except Exception as e:
                message = {
                    "msg": "error showBooks POST",
                    "error": e
                }
                return jsonify(message)
        else:
            try:
                return db.showBooks()
            except Exception as e:
                message = {
                    "msg": "error showbooks GET",
                    "error": e
                }
                return jsonify(message)
    except Exception as e:
        message = {
            "msg": e
        }
        return jsonify(message)

if __name__ == "__main__":
    db = Database()
    app.run(port=5000, debug=True)

