from mongoengine import connect, Document, StringField

class Books(Document):
    nama = StringField(required=True, max_length=255)
    pengarang = StringField(required=True, max_length=255)
    tahunterbit = StringField(required=True, max_length=255)
    genre = StringField(required=True, max_length=255)

class Database:
    def __init__(self):
        try:
            self.connection = connect(
                db="perpustakaan",
                host="localhost",
                port=27017
            )
        except Exception as e:
            print(f"kesalahan koneksi: {e}")
    
    def showBooks(self):
        try:
            return Books.objects().to_json()
        except Exception as e:
            print(f"kesalahan function showBooks: {e}")

    def showBooksByParams(self, **params):
        try:
            if "id" in params.keys():
                return Books.objects().get(id=params["id"]).to_json()
            if "nama" in params.keys():
                return Books.objects(nama__icontains=params["nama"]).to_json()
        except Exception as e:
            print(f"kesalahan function showBooksById: {e}")